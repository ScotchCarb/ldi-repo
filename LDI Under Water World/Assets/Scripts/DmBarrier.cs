using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DmBarrier : MonoBehaviour, IGate
{
    [TextArea]
    public string tooltipText;
    public Key.KeyType type;

    public bool Locked { get; private set; } = true;
    public GameObject lockAltar;

    void Start()
    {
        IbWorldMap.Instance.AddInteraction(gameObject);  
    }
 
    public string Inspect()
    {
        return tooltipText;
    }

    public bool Unlock()
    {
        if(lockAltar.GetComponent<DmGate>().Locked == true)  //checks that the gate being interacted with is locked
        {
            if(Interaction.Instance.HasKey(type) == true) //checks that you have the key
            {                
                string message = "Take the key to the Altar!!";
                ToolTip_fix.Instance.Toggle(message);
                return true;
            }
            else
            {
                //Tooltip "you are missing the key"                
                string message = "You need to find the key then take it to the Altar!";
                ToolTip_fix.Instance.Toggle(message);
                //Debug.Log("Key is missing");
            }
        }
        else
        {
            //tooltip "already unlocked" message
            tooltipText = "This gate was already unlocked";
            //Debug.Log("is Already Unlocked");
        }
        return false;
    }
}
