using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public GameObject galleryPanel;
    public GameObject infoPanel;

    private Dropdown galleryDropdown;
    private RawImage galleryDisplay;
    private List<Texture2D> screenshots = new List<Texture2D>();

    private void Awake()
    {
        galleryDropdown = galleryPanel.GetComponentInChildren<Dropdown>();
        galleryDisplay = galleryPanel.GetComponentInChildren<RawImage>();
        galleryDropdown.ClearOptions();

        #region Confirm Gallery Folder Exists

        //check for gallery foldier, create if it doesn't already exist
        string galleryFolder = Application.dataPath + "/Resources/PhotoGallery";
        if (Directory.Exists(galleryFolder) == false)
        {
            Directory.CreateDirectory(galleryFolder);
        }

        #endregion

        DirectoryInfo dir = new DirectoryInfo(galleryFolder);
        FileInfo[] screenshotInfos = dir.GetFiles("UWW_?*.png");
        if(screenshotInfos.Length > 0)
        {
            foreach (FileInfo info in screenshotInfos)
            {
                galleryDropdown.options.Add(new Dropdown.OptionData(info.Name));//add a dropdown option for this file
                Texture2D texture = new Texture2D(960, 540);//create a new texture for the screenshot
                texture.LoadImage(File.ReadAllBytes(info.FullName));//load the image from the file as a texture
                screenshots.Add(texture);//add the texture to our list of screenshots
            }
            galleryDisplay.texture = screenshots[0];
        }
        else
        {
            galleryDisplay.gameObject.SetActive(false);
        }

    }
    void Start()
    {
        galleryPanel.SetActive(false);
        infoPanel.SetActive(false);
    }

    IEnumerator LoadGame()
    {
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene(1);
    }

    public void StartGame() 
    {
        StartCoroutine(LoadGame());
    }

    public void Info()
    {
        if(galleryPanel.activeSelf == true)
        {
            galleryPanel.SetActive(false);
        }
        infoPanel.SetActive(!infoPanel.activeSelf);
    }

    public void Gallery()
    {
        if (infoPanel.activeSelf == true)
        {
            infoPanel.SetActive(false);
        }
        galleryPanel.SetActive(!galleryPanel.activeSelf);
    }

    public void SelectGalleryImage()
    {
        if(screenshots.Count > 0)
        {
            if(galleryDisplay.gameObject.activeSelf == false)
            {
                galleryDisplay.gameObject.SetActive(true);
            }
            galleryDisplay.texture = screenshots[galleryDropdown.value];
        }
    }

    public void QuitToDesktop()
    {
        //Debug.Log("Quitted game bye!!");
        Application.Quit();
    }
}
