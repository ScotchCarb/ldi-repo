using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lh_FlockAgent : Fish
{
    public float viewAngle;
    public float smoothDamp;
    public LayerMask obstacleMask;
    public Vector3[] collisionRays;

    private float speed;
    private Vector3 velocity;
    private Vector3 currentAvoidance;
    private Lh_FlockManager flockInstance;
    private List<Lh_FlockAgent> cohesionNeighbours = new List<Lh_FlockAgent>();
    private List<Lh_FlockAgent> separationNeighbours = new List<Lh_FlockAgent>();
    private List<Lh_FlockAgent> alignmentNeighbours = new List<Lh_FlockAgent>();

    public void Initialize(Lh_FlockManager manager, float initSpeed)
    {
        flockInstance = manager;
        speed = initSpeed;
    }

    /// <summary>
    /// This method calculate and applies movement for the agent on a frame by frame basis.
    /// </summary>
    public void Move()
    {
        FindNeighbours();
        calculateSpeed();

        Vector3 cohesion = Cohese() * flockInstance.cohesion;
        Vector3 separation = Separation() * flockInstance.separation;
        Vector3 alignment = Align() * flockInstance.alignment;
        Vector3 bounds = Bounds() * flockInstance.bounds;
        Vector3 avoidance = ObstacleAvoidance() * flockInstance.obstacleAvoidance;

        Vector3 motion = cohesion + separation + alignment + bounds + avoidance; //adds all these together so it can dodge obstacles and other agents, move together as a flock and stay within the bound
        motion = Vector3.SmoothDamp(transform.forward, motion, ref velocity, smoothDamp); //Smooths the forward velocity so it doesn't have as sharp movemnt.
        motion = motion.normalized * speed; //applies speed to motion, so it can actually move.
        if(motion == Vector3.zero)
        {
            motion = transform.forward;
        }
        transform.forward = motion; 
        transform.position += motion * Time.deltaTime; //Moves the position of motion and smooths out the frames so its always the same
    }

    /// <summary>
    /// Gets the neighbours for cohesion, allignment, and seperation.
    /// </summary>
    /// <returns></returns>
    private void FindNeighbours()
    {
        cohesionNeighbours.Clear();
        separationNeighbours.Clear();
        alignmentNeighbours.Clear();

        for(int i = 0; i < flockInstance.SpawnedAgents.Length; i++)
        {
            Lh_FlockAgent agent = flockInstance.SpawnedAgents[i];
            if(agent != this)
            {
                float distanceSqr = Vector3.SqrMagnitude(agent.transform.position - transform.position);
                if(distanceSqr <= flockInstance.cohesionDistance * flockInstance.cohesionDistance) //checks if distanceSqr is less than or equal to cohesionDistance * 2
                {
                    cohesionNeighbours.Add(agent); //Adds cohesionNeighbours to the agent
                }
                if(distanceSqr <= flockInstance.separationDistance * flockInstance.separationDistance)  //checks if distanceSqr is less than or equal to separationDistance * 2
                {
                    separationNeighbours.Add(agent); //Adds separeationNeighbours to the agent
                }
                if(distanceSqr <= flockInstance.alignmentDistance * flockInstance.alignmentDistance)  //checks if distanceSqr is less than or equal to alignmentDistance * 2
                {
                    alignmentNeighbours.Add(agent); //Adds alignmentNeighbours to the agent
                }
            }
        }
    }

    /// <summary>
    /// Calculates average speed of agent based on neighbours
    /// </summary>
    /// <returns></returns>
    private void calculateSpeed()
    {
        if(cohesionNeighbours.Count == 0)
        {
            return;
        }
        speed = 0;
        for(int i = 0; i < cohesionNeighbours.Count; i++) //if i is less than cohessionNeighbbours count it add 1 to i
        {
            speed += cohesionNeighbours[i].speed; //Adds cohesionNeighbours speed to speed
        }
        speed /= cohesionNeighbours.Count; //devides speed by the amount of agents in cohesionNeighbours
        speed = Mathf.Clamp(speed, flockInstance.speed.x, flockInstance.speed.y); //clamps the speed by the flockInstance speed
    }

    /// <summary>
    /// Generate the cohesion vector on a frame by frame basic.
    /// Subtracts the agents position by the average cohese position
    /// </summary>
    /// <returns></returns>
    private Vector3 Cohese()
    {
        Vector3 vector = Vector3.zero;
        int neighboursInView = 0;
        if (cohesionNeighbours.Count == 0) //If there is no cohesionNeighbours nears it will return vector
        {
            return vector;
        }
        for(int i = 0; i < cohesionNeighbours.Count; i++) //Will count up if separationNeighbour is less than i
        {
            if(VectorInView(cohesionNeighbours[i].transform.position) == true)
            {
                neighboursInView++;
                vector += cohesionNeighbours[i].transform.position; //Adds agents position with cohesionNeighbours position
            }
        }
        if(neighboursInView == 0)
        {
            return vector;
        }
        vector /= neighboursInView; 
        vector -= transform.position; 
        return vector.normalized;
    }

    /// <summary>
    /// Generates the separation vector on a frame by frame basis.
    /// subtracts the agents position by the other agents position so they dont hit each other 
    /// </summary>
    /// <returns></returns>
    private Vector3 Separation()
    {
        Vector3 vector = transform.forward;
        if(separationNeighbours.Count == 0) //If there is no separationNeighbours nears it will return vector
        {
            return vector;
        }
        int neighbourInView = 0;
        for(int i = 0; i < separationNeighbours.Count; i++) //Will count up if separationNeighbour is less than i
        {
            if(VectorInView(separationNeighbours[i].transform.position) == true)
            {
                neighbourInView++;
                vector += transform.position - separationNeighbours[i].transform.position; //Will go in opposite direction from the neighbour by subtrating the agents position by separationNeighbours.
            }
        }
        vector /= neighbourInView;
        return vector.normalized;
    }

    /// <summary>
    /// Generates the allignment vector on a frame by frame basis.
    /// Aligns the agent by taking its position and adding the other agents position
    /// </summary>
    /// <returns></returns>
    private Vector3 Align()
    {
        Vector3 vector = transform.forward;
        if (alignmentNeighbours.Count == 0) //If there is no alignmentneighbours nears it will return vector
        {
            return vector; 
        }
        int neighbourInView = 0;
        for (int i = 0; i < alignmentNeighbours.Count; i++) //Will count up if alignmentNeighbour is less than i
        {
            if (VectorInView(alignmentNeighbours[i].transform.position) == true)
            {
                neighbourInView++;
                vector += alignmentNeighbours[i].transform.position; //Adds agents position with alignmentNeighbours position
            }
        }
        vector /= neighbourInView;
        return vector.normalized;
    }
   
    /// <summary>
    /// Generates a vector that keeps the agent close to the flock object.
    /// </summary>
    /// <returns></returns>
       private Vector3 Bounds()
    {
        Vector3 centerOffset = flockInstance.transform.position - transform.position;
        bool withinBounds = (centerOffset.magnitude >= flockInstance.boundsDistance * 0.9);
        if(withinBounds == true)
        {
            return centerOffset.normalized;
        }
        return Vector3.zero;
    }

    /// <summary>
    /// Generates an obstacle avoidance vector on a frame by frame basis.
    /// </summary>
    /// <returns></returns>
    private Vector3 ObstacleAvoidance()
    {
        Vector3 vector = Vector3.zero;
        if(Physics.Raycast(transform.position, transform.forward, out RaycastHit hit, flockInstance.obstacleDistance, obstacleMask) == true) //Checks if raycasts are being hit
        {
            vector = AvoidObstacle(); //Makes vector = to AvoidObstrace
        }
        else
        {
            currentAvoidance = Vector3.zero;
        }
        return vector;
    }

    /// <summary>
    /// Checks all collision rays for collisions.
    /// Returns the most suitable direction to move in.
    /// <summary>
    /// <returns></returns>
    private Vector3 AvoidObstacle()
    {
        if(currentAvoidance != Vector3.zero) //Checks if currentAvoidnace does not equal zero
        {
            if(Physics.Raycast(transform.position, transform.forward, out RaycastHit hit,flockInstance.obstacleAvoidance, obstacleMask) == false) //Checks if raycasts are not being hit
            {
                return currentAvoidance;
            }
        }
        float maxDist = int.MinValue;
        Vector3 dir = Vector3.zero; //Resets dir to zero
        for(int i = 0; i < collisionRays.Length; i++) //if i is less than collisionsRays length it will add 1 to i
        {
            Vector3 currentDir = transform.TransformDirection(collisionRays[i].normalized);
            if(Physics.Raycast(transform.position, currentDir, out RaycastHit hit, flockInstance.obstacleAvoidance, obstacleMask) == true) //Checks if raycasts are being hit
            {
                float sqrDist = (hit.point - transform.position).sqrMagnitude; //Takes the raycast hit point and subtract the position
                if(sqrDist > maxDist) //Checks if sqrDist is more than maxDist
                {
                    maxDist = sqrDist; //Makes maxDist = to sqrDist
                    dir = currentDir; //Makes dir = to currentDir
                }
            }
            else
            {
                dir = currentDir; //Makes dir = to currentDir
                currentAvoidance = currentDir.normalized; //Make currentAvoidance = to currentDir
                return dir.normalized;
            }
        }
        return dir.normalized;
    }

    /// <summary>
    /// Returns true if the given position is within agents view radius.
    /// </summary>
    private bool VectorInView(Vector3 position)
    {
        return Vector3.Angle(transform.forward, position - transform.position) <= viewAngle;
    }
    
}