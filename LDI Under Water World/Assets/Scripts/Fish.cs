using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Fish : MonoBehaviour, IFauna
{
    [TextArea]
    public string tooltipText;
    public string nameText;

    public bool isSolo;

    public string Inspect()
    {
        return tooltipText;
    }

    public void Photograph(string folderPath = null)
    {
        if (folderPath != null)
        {
            string date = System.DateTime.Now.Day.ToString() + System.DateTime.Now.Month.ToString() + System.DateTime.Now.TimeOfDay.ToString().Replace(':', '-');
            ScreenCapture.CaptureScreenshot(folderPath + $"/UWW_{date}.png", 2);
        }

        //if this is target fish, and target fish has not been found
        if (GameManager.Instance.TargetFound == false && (Object)GameManager.Target == this)
        {
            GameManager.Instance.TargetFound = true;
        }

    }
            

}
