using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class DmInteractionMenu : MonoBehaviour
{
    public Image optionA;
    public Image optionB;
    public Text optionBText;

    private IInteraction currentInteraction;
    private int optionIndex = -1;
    private float timer = -1;

    public static DmInteractionMenu Instance { get; private set; }

    private void Awake()
    {
        //singleton initialization
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            enabled = false;
        }
        //photo gallery folder construction
        if (Directory.Exists(Application.dataPath + "/Resources/PhotoGallery") == false)
        {
            Directory.CreateDirectory(Application.dataPath + "/Resources/PhotoGallery");
        }
    }

   void UpdateUISelection()
    {
        Color selectedOrange; //sets the colors of the boxes to go with the specifics we decided for the project, base white/grey was boring.
        Color unselectedBlue;

        ColorUtility.TryParseHtmlString("#FFA047", out selectedOrange);
        ColorUtility.TryParseHtmlString("#3A74A6", out unselectedBlue);
        switch (optionIndex)
        {
            case 0:
                optionA.color = selectedOrange;
                optionB.color = unselectedBlue;
                break;
            case 1:
                optionA.color = unselectedBlue;
                optionB.color = selectedOrange;
                break;
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        Deactivate();
    }

    // Update is called once per frame
    void Update()
    {
       if(gameObject.activeSelf == true && timer == -1)
        {
            int axis = Mathf.FloorToInt(Input.GetAxis("Menu Selection"));
            if(axis != 0)
            {
                timer = 0;
                optionIndex += axis;
                if(optionIndex < 0)
                {
                    optionIndex = 1;
                }
                else if(optionIndex > 1)
                {
                    optionIndex = 0;
                }
                UpdateUISelection();
            }
        } 
       if(timer > - 1)
        {
            timer += Time.deltaTime;
            if(timer > 0.25f)
            {
                timer = -1f;
            }
        }
    }

    void Photograph(IFauna fauna) // this is to take photographs.
    {
        string gallery = Application.dataPath + "/Resources/PhotoGallery"; //sets the datapath for where these files will go
        DirectoryInfo dir = new DirectoryInfo(gallery);
        FileInfo[] files = dir.GetFiles("*.png");
        int count = 0;
        foreach (FileInfo info in files)
        {
            if(info.Name.Contains("UWW_") == true)  //checks the count of images and adds 1 more
            {
                count++;  
            }
        }

        if(count < 30)  //if less than 30 adds a photograph to the gallery
        {
            fauna.Photograph(gallery);
        }
        else  //if more does not
        {
            fauna.Photograph();
        }

    }

    bool GetInteractionAsType<InteractionType>(out InteractionType target) where InteractionType : IInteraction
    {
        if(currentInteraction is InteractionType interaction)
        {
            target = interaction;
            return true;
        }
        target = default;
        return false;
    }

    public void Activate(IInteraction interaction)  //lists the interactions and their texts to show what is required
    {
        currentInteraction = interaction;
        if (GetInteractionAsType(out IFauna fauna) == true)
        {
            optionBText.text = "Photograph";
            optionBText.color = Color.black;

        }
        else if (GetInteractionAsType(out IKey key) == true)
        {
            optionBText.text = "Pick Up";
            optionBText.color = Color.black;
        }
        else if (GetInteractionAsType(out IGate gate) == true)
        {
            optionBText.text = "Unlock";
            optionBText.color = Color.black;
        }
        else if (GetInteractionAsType(out IShip ship) == true)
        {
            optionBText.text = "Leave";
            optionBText.color = Color.black;
        }
        optionIndex = 0;
        UpdateUISelection();
        gameObject.SetActive(true);
    }

    public void Deactivate()
    {
        if(optionIndex == 0)
        {
            //pass current interaction to tooltip
            ToolTip_fix.Instance.Toggle(currentInteraction.Inspect());
        }
        else if(optionIndex == 1)
        {
            if (GetInteractionAsType(out IFauna fauna) == true)
            {
                Photograph(fauna);
            }
            else if (GetInteractionAsType(out IKey key) == true)
            {
                key.Pickup();
            }
            else if(GetInteractionAsType(out IGate gate) == true)
            {
                gate.Unlock();
            }
            else if(GetInteractionAsType(out IShip ship) == true)
            {
                ship.Leave();
            }
        }
        gameObject.SetActive(false);
        optionIndex = -1;
        currentInteraction = null;
    }
}
