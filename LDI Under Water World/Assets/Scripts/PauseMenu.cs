using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour
{

    public static bool GameIsPaused = false;

    public GameObject pauseMenuUI;
    public GameObject optionsMenuUI;
    public GameObject resumeButton;
    public GameObject quitButton;
    public GameObject optionsButton;
    public GameObject mainButton;
    

    void Update()
    {
        if (Input.GetKeyDown("escape"))
        {
            if (GameIsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
        
    }

    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        optionsMenuUI.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;
        MouseLook.Instance.LookEnabled = true;
        MouseLook.Instance.CursorToggle = false;
    }

    private void Pause()
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
        MouseLook.Instance.LookEnabled = false;
        MouseLook.Instance.CursorToggle = true;

       
    }

    public void PromptWindow()
    {
        resumeButton.GetComponent<Button>().interactable = false;
        quitButton.GetComponent<Button>().interactable = false;
        optionsButton.GetComponent<Button>().interactable = false;
        mainButton.GetComponent<Button>().interactable = false;
    }

    public void CancelPrompt()
    {
        resumeButton.GetComponent<Button>().interactable = true;
        quitButton.GetComponent<Button>().interactable = true;
        optionsButton.GetComponent<Button>().interactable = true;
        mainButton.GetComponent<Button>().interactable = true;
    }

   
    public void MainMenu()
    {
        //goes to main menu
        SceneManager.LoadScene("MainMenu");
        Time.timeScale = 1f;
        GameIsPaused = false;

    }

    
    public void QuitGame()
    {
        //quits game
        //Debug.Log("Quitted the game bye");
        Application.Quit();
    }

}
