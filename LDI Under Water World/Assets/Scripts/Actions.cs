using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Actions : MonoBehaviour
{
    public float boosterMultiplier = 5;
    public float boostTime = 3;
    public float sonarTime = 3;
    public float sonarRadius = 15;
    public int iconLimit = 5;
    public LayerMask interactionLayer;
    public GameObject sonarIconPrefab;

    public static Actions Instance { get; private set; }

    private float sonarTimer = -1;
    private float boostTimer = -1;
    private List<GameObject> spawnedSonarIcons = new List<GameObject>();

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            enabled = false;
        }
    }

    void Start()
    {
        //spawn in a number of sonar icons equal to the defined limit, set their position to zero, their parent to the UI canvas and then make them not active
        for(int i = 0; i < iconLimit; i++)
        {
            spawnedSonarIcons.Add(Instantiate(sonarIconPrefab, Lh_ActionMenu.Instance.transform.root, false));
            spawnedSonarIcons[i].transform.position = Vector3.zero;
            spawnedSonarIcons[i].transform.SetAsFirstSibling();
            spawnedSonarIcons[i].SetActive(false);
        }
        //Deactivate map
        Map(false);

       
    }

    void Update()
    {
        //if the world map is not active
        if (IbWorldMap.Instance.legend.activeSelf == false) 
        {
            if (Lh_ActionMenu.Instance.gameObject.activeSelf == false)
            {
                if (DmInteractionMenu.Instance.gameObject.activeSelf == false)
                {
                    if (Input.GetButtonDown("Action") == true)
                    {
                        Lh_ActionMenu.Instance.Activate();
                    }
                }
            }
            else
            {
                if (Input.GetButtonUp("Action") == true)
                {
                    int selection = Lh_ActionMenu.Instance.Deactivate();
                    switch (selection)
                    {
                        case 0:
                            Boost(true);
                            break;
                        case 1:
                            Sonar(true);
                            break;
                        case 2:
                            Map(true);
                            break;
                    }
                }
            }
        }
        else if (Input.GetButtonDown("Action") == true)
        {
            Map(false);
        }
        

        if(boostTimer > -1)//boost timer
        {
            boostTimer += Time.deltaTime;
            if(boostTimer > boostTime)
            {
                Boost(false);
            }
        }

        if(sonarTimer > -1)
        {
            Collider[] closeInteractions = Physics.OverlapSphere(transform.position, sonarRadius, interactionLayer);
            int interactionCount = closeInteractions.Length;
            for(int i = 0; i < iconLimit; i++)
            {
                if(i < interactionCount)
                {
                    Vector3 screenPos = Camera.main.WorldToScreenPoint(closeInteractions[i].transform.position);
                    if(screenPos.z > 0) //if icon is visible on the screen - negative z object means object is not on the screen
                    {
                        spawnedSonarIcons[i].transform.position = screenPos;
                        if(spawnedSonarIcons[i].activeSelf == false)
                        {
                            spawnedSonarIcons[i].SetActive(true); //turn on icon
                        }
                    }
                    else if(spawnedSonarIcons[i].activeSelf == true) //if icon is active and no on screen
                    {
                        spawnedSonarIcons[i].SetActive(false); //turn off icon
                    }
                }
                else if (spawnedSonarIcons[i].activeSelf == true) //if icon is active but there are no corresponding interactions available.
                {
                    spawnedSonarIcons[i].SetActive(false); //turn off icon
                }
            }

            sonarTimer += Time.deltaTime;
            if(sonarTimer > sonarTime)
            {
                Sonar(false);
            }
        }
    }

    void Boost(bool toggle)
    {
        if(toggle == true) //turn on functionality
        {
            if(boostTimer == -1)
            {
                boostTimer = 0;
                CharacterMove.Instance.speed *= boosterMultiplier;
            }
        }
        else //turn off functionality
        {
            if (boostTimer > -1)
            {

                boostTimer = -1;
                CharacterMove.Instance.speed /= boosterMultiplier;
            }
        }
    }

    void Sonar(bool toggle)
    {
        if(toggle == true) //activate
        {
            if(sonarTimer == -1)
            {
                sonarTimer = 0;
            }
        }
        else //deactivate
        {
            sonarTimer = -1;
            if(spawnedSonarIcons.Count > 0)
            {
                foreach(GameObject go in spawnedSonarIcons)
                {
                    go.SetActive(false);
                }
            }
        }
    }

    void Map(bool toggle) 
    {
        if(toggle == true) //activate
        {
           
            CharacterMove.Instance.enabled = false;//turn off player movement
            MouseLook.Instance.enabled = false;//turn off mouse look
            Interaction.Instance.enabled = false;//turn off interaction
            Sonar(false);//deactivate sonar
            Boost(false);//deactivate boost
            IbWorldMap.Instance.Activate(); //activate map
        }
        else //deactivate
        { 
            if (GameManager.Instance.cutSceneCamera.gameObject.activeSelf != true)
            {

                CharacterMove.Instance.enabled = true;//turn on player movement
                MouseLook.Instance.enabled = true;//turn on mouse look
                Interaction.Instance.enabled = true;//turn on interaction
            }
            IbWorldMap.Instance.Deactivate(); //deactivate map
        }
    }
}
