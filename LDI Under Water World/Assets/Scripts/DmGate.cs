using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DmGate : MonoBehaviour, IGate
{
    [TextArea]
    public string tooltipText;
    public Key.KeyType type;

    public bool Locked { get; private set; } = true;
    public GameManager manager;
    public GameObject gate;
    public GameObject altarKey;

    private Vector3 gateOpen;
    private float openTime = 2.5f;
    private void Awake()
    {
        gateOpen = Vector3.zero;
    }
    void Start()
    {
        IbWorldMap.Instance.AddInteraction(gameObject);  
    }
 
    private void Update()
    {
        if (Locked == false && (gate.transform.localScale.z > 1))
        {
            gate.transform.localScale = Vector3.Lerp(gate.transform.localScale, gateOpen, openTime * Time.deltaTime); //this scales the object down (the gate) to the pre defined zero state transforming it from its current size, over the variable time of openTime
        }

        if (gate.transform.localScale.z < 1)
        {
            gate.active = false;
        }
    }

    public string Inspect()
    {
        return tooltipText;
    }

    public bool Unlock()
    {
        if(Locked == true)  //checks that the gate being interacted with is locked
        {
            if(Interaction.Instance.HasKey(type) == true) //checks that you have the key
            {
                Locked = false;
                //Debug.Log("Cutscene " + type + "has been triggered");
                if( type == Key.KeyType.a)
                {
                    manager.StartCoroutine(manager.PlayCutScene(manager.gateACutScene));  //plays the cutscene for gate A
                    altarKey.SetActive(true);
                }
                else
                {
                    manager.StartCoroutine(manager.PlayCutScene(manager.gateBCutScene));  //plays the cutscene
                    altarKey.SetActive(true);
                }

                return true;
            }
            else
            {
                //Tooltip "you are missing the key"
                string message = "You need to find the key!";
                ToolTip_fix.Instance.Toggle(message);


                //Debug.Log("Key is missing");
            }
        }
        else
        {
            //tooltip "already unlocked" message
            string message = "This gate was already unlocked";
            ToolTip_fix.Instance.Toggle(message);

            //Debug.Log("is Already Unlocked");
        }
        return false;
    }
}
