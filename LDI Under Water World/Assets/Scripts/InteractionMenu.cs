using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;


public class InteractionMenu : MonoBehaviour
{
    public Image optionA, optionB;
    public Text optionBText;

    private IInteraction currentInteraction;
    private int optionIndex = -1;
    private float timer = -1;

    public static InteractionMenu Instance { get; private set; }

    private void Awake()
    {
        //singleton initialization
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            enabled = false;
        }
        //Photo gallery folder construction
        if(Directory.Exists(Application.dataPath + "/Resources/PhotoGallery") == false)
        {
            Directory.CreateDirectory(Application.dataPath + "/Resources/PhotoGallery");
        }
    }

    void Start()
    {
        Deactivate();
    }

    void Update()
    {
        if(gameObject.activeSelf == true && timer == -1)
        {
            int axis = Mathf.FloorToInt(Input.GetAxis("Menu Selection"));
            if(axis != 0)
            {
                timer = 0;
                optionIndex += axis;
                if(optionIndex < 0)
                {
                    optionIndex = 1;
                }
                else if (optionIndex > 1)
                {
                    optionIndex = 0;
                }
                UpdateUISelection();
            }
        }

        if(timer > -1)
        {
            timer += Time.deltaTime;
            if(timer > 0.25f)
            {
                timer = -1;
            }
        }
    }

    void Photograph(IFauna fauna) //nickelback
    {
        string gallery = Application.dataPath + "/Resources/PhotoGallery";
        DirectoryInfo dir = new DirectoryInfo(gallery);
        FileInfo[] files = dir.GetFiles("*.png");
        int count = 0;
        foreach (FileInfo info in files)
        {
            if (info.Name.Contains("UWW_"))
            {
                count++;
            }
        }

        if(count < 30)
        {
            fauna.Photograph(gallery);
        }
        else
        {
            fauna.Photograph();
        }
    }

    bool GetCurrentInteractionAsType<InteractionType>(out InteractionType target) where InteractionType : IInteraction
    {
        if(currentInteraction is InteractionType interaction)
        {
            target = interaction;
            return true;
        }
        target = default;
        return false;
    }

    public void Activate(IInteraction interaction) 
    {
        currentInteraction = interaction;
        if (GetCurrentInteractionAsType(out IFauna fauna) == true)
        {
            optionBText.text = "Photograph";
            ;
        }
        else if (GetCurrentInteractionAsType(out IKey key) == true)
        {

            optionBText.text = "Pickup";
            

        }
        else if (GetCurrentInteractionAsType(out IGate gate) == true)
        {

            optionBText.text = "Unlock";
            

        }
        else if (GetCurrentInteractionAsType(out IShip ship) == true)
        {
            optionBText.text = "Head Home";
           
        }
        optionIndex = 0;
        UpdateUISelection();
        gameObject.SetActive(true);
    }

    public void Deactivate()
    {
        if(optionIndex == 0)
        {
            //pass current interaction to tooltip
            ToolTip_fix.Instance.Toggle(currentInteraction.Inspect());
        }
        else if(optionIndex == 1)
        {
            if(GetCurrentInteractionAsType(out IFauna fauna) == true)
            {
                Photograph(fauna);
            }
            else if(GetCurrentInteractionAsType(out IKey key) == true)
            {
                key.Pickup();
            }
            else if (GetCurrentInteractionAsType(out IGate gate) == true)
            {
                gate.Unlock();
            }
            else if(GetCurrentInteractionAsType(out IShip ship) == true)
            {
                ship.Leave();
            }
        }
        gameObject.SetActive(false);
        optionIndex = -1;
        currentInteraction = null;
    }

    void UpdateUISelection()
    {

        Color selectedOrange;
        Color unselectedBlue;

        ColorUtility.TryParseHtmlString("#FFA047", out selectedOrange);
        ColorUtility.TryParseHtmlString("#3A74A6", out unselectedBlue);


        switch (optionIndex)
        {
            case 0:
                optionA.color = selectedOrange;
                optionB.color = unselectedBlue;
                break;
            case 1:
                optionB.color = selectedOrange;
                optionA.color = unselectedBlue;
                break;

        }
    }
}
