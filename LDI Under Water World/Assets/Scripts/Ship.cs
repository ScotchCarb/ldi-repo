using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using FiniteStateMachine;
using UnityEngine.SceneManagement;

public class Ship : MonoBehaviour, IShip
{
    [TextArea]
    public string tooltipText;
    public Vector3 playerSpawnOffset;
    public Bounds boundBox;
    public ShipWait waitState;
    public ShipMove moveState;

    public StateMachine StateMachine { get; private set; }
    public NavMeshAgent Agent { get; private set; }

    public bool isStarted = false;

    /*private void Awake()
    {
        Agent = GetComponent<NavMeshAgent>();
        waitState = new ShipWait(this) { waitTime = waitState.waitTime };
        moveState = new ShipMove(this);
        StateMachine = new StateMachine(waitState);
    }*/

    void Start()
    {
        CharacterMove.Instance.MoveToPosition(transform.position + playerSpawnOffset);
        IbWorldMap.Instance.AddInteraction(gameObject);
    }

    private void Update()
    {
        if (isStarted == true)
        {
            StateMachine.OnUpdate();
        }
    }

    public void ShipStart()
    {
        Agent = GetComponent<NavMeshAgent>();
        waitState = new ShipWait(this) { waitTime = waitState.waitTime };
        moveState = new ShipMove(this);
        StateMachine = new StateMachine(waitState);
        isStarted = true;
    }

    public string Inspect()
    {
        return tooltipText;
    }

    public void Leave()
    {
        //if targetfish found
        if (GameManager.Instance.TargetFound == true)
        {
            //return to main menu
            MouseLook.Instance.CursorToggle = true;
            SceneManager.LoadScene(0);
        }
        else 
        {
            //Tooltip 'must find target'
            string message = $"You need to find the target fish '{GameManager.Instance.targetText.text}' before you can leave!!";
            ToolTip_fix.Instance.Toggle(message);
        }


    }

    public void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(boundBox.center, boundBox.size);
        if(StateMachine != null && StateMachine.CurrentState != null)
        {
            StateMachine.GetCurrentStateAsType<ShipState>().DrawGizmos();
        }
    }
}

public abstract class ShipState : IState
{

    public Ship Instance { get; private set; }

    public ShipState(Ship instance) 
    {
        Instance = instance;
    }

    public virtual void OnEnter(){ }

    public virtual void OnExit(){ }

    public virtual void OnUpdate(){}

    public virtual void DrawGizmos() { }
}

[System.Serializable]
public class ShipWait : ShipState
{
    public Vector2 waitTime = new Vector2(1, 10);

    private float time = 0;
    private float timer = -1;
    public ShipWait(Ship instance) : base(instance)
    {
    }

    public override void OnEnter()
    {
        timer = 0;
        time = Random.Range(waitTime.x, waitTime.y);
    }

    public override void OnUpdate()
    {
        if(timer > -1)
        {
            timer += Time.deltaTime;
            if(timer > time)
            {
                Instance.StateMachine.SetState(Instance.moveState);
            }
        }
    }

    public override void OnExit()
    {
        timer = -1;
    }
}

[System.Serializable]
public class ShipMove : ShipState
{
    private Vector3 targetPos;
    public ShipMove(Ship instance) : base(instance)
    {
    }

    public override void OnEnter()
    {
        //get random target position in bounds
        targetPos = GetRandomPosInBounds();
        Instance.Agent.SetDestination(targetPos);        
    }

    public override void OnUpdate()
    {
        if(Vector3.Distance(Instance.transform.position, targetPos)< Instance.Agent.stoppingDistance)
        {
            Instance.StateMachine.SetState(Instance.waitState);
        }
    }

    public override void DrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(targetPos, 1);
    }

    public Vector3 GetRandomPosInBounds()
    {
        return new Vector3(Random.Range(-Instance.boundBox.extents.x, Instance.boundBox.extents.x), Instance.transform.position.y, Random.Range(-Instance.boundBox.extents.z, Instance.boundBox.extents.z));
    }
}

