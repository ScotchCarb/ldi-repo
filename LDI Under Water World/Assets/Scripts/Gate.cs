using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gate : MonoBehaviour, IGate
{
    [TextArea]
    public string toolTipText;
    public Key.KeyType type;
    public GameObject gate;
    public GameManager manager;
    public GameObject altarKey;
    
    public bool Locked { get; private set; } = true;

    private Vector3 gateOpen;
    private float openTime = 2.5f;
    private void Awake()
    {
        gateOpen = Vector3.zero;
    }
    void Start()
    {
        IbWorldMap.Instance.AddInteraction(gameObject);

    }

    private void Update()
    {
        if (Locked == false && (gate.transform.localScale.z > 0))
        { 
            gate.transform.localScale = Vector3.Lerp(gate.transform.localScale, gateOpen, openTime * Time.deltaTime);
        }
    }
    public string Inspect()
    {
        return toolTipText;
    }

    public bool Unlock()
    {
        if(Locked == true)
        {
            if(Interaction.Instance.HasKey(type) == true)
            {
                Locked = false;
                //Debug.Log("Cutscene " + type + " has been triggered!!");
                if(type == Key.KeyType.a)
                {
                    
                    altarKey.SetActive(true);
                }
                else
                {
                    
                    altarKey.SetActive(true);
                }

                return true;
            }
            else
            {
                //tooltip "you are missing the key"
                string message = "You need to find the key!";
                ToolTip_fix.Instance.Toggle(message);

                //Debug.Log("Key is missing");

            }
        }
        else
        {
            //tooltip "already unlocked"
            string message = "Already unlocked!";
            ToolTip_fix.Instance.Toggle(message);

            //Debug.Log("Already unlocked!");
        }
        return false;
    }

}
