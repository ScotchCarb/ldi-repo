using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : MonoBehaviour, IKey
{
    public enum KeyType { a, b }

    public KeyType type;
    [TextArea]
    public string tooltipText;

    void Start()
    {
        IbWorldMap.Instance.AddInteraction(gameObject);
    }

    public void Pickup()
    {
        //if key is successfuly picked up
        if(Interaction.Instance.AddKey(type) == true)
        {
            //turn object off
            gameObject.SetActive(false);
        }
    }

    public string Inspect()
    {
        return tooltipText;
    }
}
