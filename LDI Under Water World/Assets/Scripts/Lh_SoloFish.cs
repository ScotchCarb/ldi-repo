using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lh_SoloFish : Fish
{
    public Bounds boundBox;
    public float smoothDamp;
    public Vector3[] collisionRays;
    public float boundsDistance;
    public float obstacleDistance;
    public float boundsWeight;
    public float obstacleWeight;
    public float targetWeight;
    public float speed;
    public LayerMask obstacleMask;

    private Vector3 currentVeloctiy;
    private Vector3 targetPosition;


    // Update is called once per frame
    void Update()
    {
        if(Random.Range(0, 1000) < 5) //Runs when the range is less than 5
        {
            targetPosition = GetRandomPosInBounds(); //Gets a new target position from GetRandomPisInBounds

        }
        MoveStep();
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(boundBox.center, boundBox.size); //Draws a red box for bounds
        Gizmos.DrawWireSphere(targetPosition, 1); //Draws a sphere at the target position
    }

    /// <summary>
    /// Sets a new target position within the bounds 
    /// </summary>
    /// <returns></returns>
    private Vector3 GetRandomPosInBounds()
    {
        Vector3 randomCoords = new Vector3(Random.Range(-boundBox.extents.x, boundBox.extents.x),
            Random.Range(-boundBox.extents.y, boundBox.extents.y),
            Random.Range(-boundBox.extents.z, boundBox.extents.z));
        return boundBox.center + randomCoords;
    }

    /// <summary>
    /// * the Bounds, ObstacleDistance, Target position by its weight so it can avoid it more or less
    /// Moves the fish forward based on Bounds, ObstacleDistance and target position
    /// Also smooths the motion of the fish so its movement is less sharp
    /// </summary>
    private void MoveStep()
    {
        Vector3 bounds = Bounds() * boundsWeight;
        Vector3 avoidance = ObstacleDistance() * obstacleWeight;
        Vector3 target = (targetPosition - transform.position).normalized * targetWeight;
        Vector3 motion = bounds + avoidance + target; 
        motion = Vector3.SmoothDamp(transform.forward, motion, ref currentVeloctiy, smoothDamp); //Smoothes the movement
        motion = motion.normalized * speed; //Applies speed to the fish
        if(motion == Vector3.zero)
        {
            motion = transform.forward;
        }
        transform.forward = motion;
        transform.position += motion * Time.deltaTime; 
    }

    /// <summary>
    /// Calculate distance of obstacles based on if a raycast gets hit or not
    /// If raycast is hit it will go in the opposite direction of where it got hit, by subtracting distance from its position
    /// </summary>
    /// <returns></returns>
    private Vector3 ObstacleDistance()
    {
        Vector3 vector = Vector3.zero;
        float largestDistance = int.MinValue;

        for (int i = 0; i < collisionRays.Length; i++) //if i is less the collisionRays length it will then add 1 to i
        {
            Vector3 currentDir = transform.TransformDirection(collisionRays[i].normalized);
            if (Physics.Raycast(transform.position, currentDir, out RaycastHit hit, obstacleDistance, obstacleMask) == true) //Checks if the raycast is being hit
            {
                Debug.DrawRay(transform.position, currentDir * obstacleDistance, Color.green);
                float sqrDistance = (hit.point - transform.position).sqrMagnitude; //sqedistance = the hit point of the raycast - transform position, then return the square length
                if(sqrDistance > largestDistance) //Check if sqrDistance is hirher than largestDistance
                {
                    largestDistance = sqrDistance; //Sets largestDistance as the sqrdistance
                    vector += currentDir; //Add currentDir to vector
                }
            }
            else
            {
                Debug.DrawRay(transform.position, currentDir * obstacleDistance, Color.red); //if raycast isnt being hit by anything, it return red
                vector += currentDir; //Add currentDir to vector

            }
        }
        return vector.normalized;
    }

    /// <summary>
    /// Creates a box in the middle of its position
    /// </summary>
    /// <returns></returns>
    private Vector3 Bounds()
    {
        Vector3 centerOffset = boundBox.center - transform.position;
        bool withinBounds = (centerOffset.magnitude >= boundsDistance * 0.9);
        if (withinBounds == true)
        {
            return centerOffset.normalized;
        }
        return Vector3.zero;
    }
}
