using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMove : MonoBehaviour
{
    public float speed = 3f;
    public Vector3 invisWalls;

    private Vector3 input;
    private Vector3 motion;
    private CharacterController controller;


    public static CharacterMove Instance { get; private set; }

    private void Awake()
    {
        //singleton setup
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            enabled = false;
        }
        controller = GetComponent<CharacterController>(); //reference the character controller
    }

    void Update()
    {
        motion = Vector3.zero;
        input = new Vector3(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Depth"), Input.GetAxisRaw("Vertical")); //get inputs
        motion += transform.forward.normalized * input.z; //forward motion
        motion += transform.right.normalized * input.x;//horizontal motion
        motion += Vector3.up.normalized * input.y;//vertical motion
        controller.Move(motion * speed * Time.deltaTime); //apply movement to player character
    }

    public void MoveToPosition(Vector3 targetPos)
    {
        controller.enabled = false;
        transform.position = targetPos;
        controller.enabled = true;
    }


  
}
