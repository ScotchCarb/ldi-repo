using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [Header("Spawn Settings")]  
    public List<GameObject> fishPrefabs;
    public List<Transform> ZoneASpawns;  //allows the setting the Zone A B and C spawns
    public List<Transform> ZoneBSpawns;
    public List<Transform> ZoneCSpawns;
    
    [Header("UI")]
    public Text targetText;
    [Header("Cut Scenes")]
    public Camera cutSceneCamera;
    public Vector3 cameraOffset;
    public PlayableDirector cutsceneDirector;  //sets the cutscenes, be it intro, gate A or gate B, or the target fish being in range.
    public PlayableAsset introCutScene;
    public PlayableAsset gateACutScene;
    public PlayableAsset gateBCutScene;
    public PlayableAsset targetFishCutScene;

    public GameObject player;
    public Transform targetFishPosition;
    public float targetDetectDist;


    public static GameManager Instance { get; private set; }
    public static IFauna Target { get; private set; }
    public bool TargetFound { get; set; } = false;
    //cutscene properties
    public bool OrbitCameraTarget { get; set; } = false;
    public bool OrbitCameraTarget2 { get; set; } = false;
    public bool ZoomCameraTarget { get; set; } = false;
    public bool gateAPlayed { get; set; } = false;
    public bool gateBPlayed { get; set; } = false;

    public Transform CameraTarget { get; private set; }



    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            enabled = false;
        }

        #region Spawn Fish
        GameObject targetFish = fishPrefabs[Random.Range(0, fishPrefabs.Count)];
        while(targetFish.GetComponent<IFauna>() == null)
        {
            targetFish = fishPrefabs[Random.Range(0, fishPrefabs.Count)];
        }
        fishPrefabs.Remove(targetFish);
        Transform spawn = ZoneCSpawns[Random.Range(0, ZoneCSpawns.Count)];
        GameObject spawnedFish = Instantiate(targetFish, spawn.transform.position, spawn.transform.rotation);

        /* This functionality isn't implemented and would require the boundBox from the various SoloFish scripts to be moved to Fish,
         * but is a better way of checking for whether the fish is a viable target for the game objective rather than 3x if statements.
         if (spawnedFish.TryGetComponent(out Fish fish))
        {
            if (fish.isSolo == true)
            {
                fish.boundBox.extents = spawn.position;
            }
        }*/
        
        if (spawnedFish.TryGetComponent(out DmSoloFish sFish) == true)
        {
            sFish.boundBox.center = spawn.position;
            //sFish.boundBox.extents = 
        }
        else if (spawnedFish.TryGetComponent(out IbSoloFish xFish) == true)
        {
            xFish.boundBox.center = spawn.position;
        }
        else if (spawnedFish.TryGetComponent(out Lh_SoloFish yFish) == true)
        {
            yFish.boundBox.center = spawn.position;
        }
        spawnedFish.name = targetFish.name;
        targetFishPosition = spawnedFish.transform;
        targetText.text = targetFish.GetComponent<Fish>().nameText;
        Target = spawnedFish.GetComponent<IFauna>();
        ZoneCSpawns.Remove(spawn);
        #endregion

        if (fishPrefabs.Count > 0  && ZoneASpawns.Count > 0)
        {
            SpawnZone(ZoneASpawns);
        }
        if (fishPrefabs.Count > 0 && ZoneBSpawns.Count > 0)
        {
            SpawnZone(ZoneBSpawns);
        }
        if (fishPrefabs.Count > 0 && ZoneCSpawns.Count > 0)
        {
            SpawnZone(ZoneCSpawns);
        }
    }

    private void Start()
    {
        if(cutsceneDirector != null && introCutScene != null)
        {
            StartCoroutine(PlayCutScene(introCutScene));
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(CameraTarget != null)
        {
            if(OrbitCameraTarget == true) //tells the camera what to do, in this care rotate around the object its set to target and to move itself to look at it
            {
                cutSceneCamera.transform.RotateAround(CameraTarget.position, CameraTarget.right, 15 * Time.deltaTime);
                cutSceneCamera.transform.LookAt(CameraTarget);
                //Debug.Log("Orbit1 is active");
            }           
        }

        if(CameraTarget != null)
        {            
            if(ZoomCameraTarget == true && Vector3.Distance(cutSceneCamera.transform.position, CameraTarget.transform.position) >= 10) //tells the camera what to do, in this case move directly towards the  target and to move itself to look at it
            {
                //Debug.Log("PROBLEMO");
                cutSceneCamera.transform.position = Vector3.MoveTowards(cutSceneCamera.transform.position, CameraTarget.transform.position, 15 * Time.deltaTime);
                cutSceneCamera.transform.LookAt(CameraTarget);
                cutSceneCamera.transform.RotateAround(CameraTarget.position, CameraTarget.up, 50 * Time.deltaTime);
            }
            //else
            //{
                //cutSceneCamera.transform.RotateAround(CameraTarget.position, CameraTarget.up, 50 * Time.deltaTime);
                //Debug.Log("PROBLEMO");
            //}
        }

        //if player is within X units of the target fish
        if (targetFishPosition != null)
        {
            if (Vector3.Distance(player.transform.position, targetFishPosition.position) < targetDetectDist && TargetFound != true ||  Input.GetKey(KeyCode.O) && Input.GetKey(KeyCode.P) && Input.GetKey(KeyCode.RightArrow))
            {
                Vector3 newOffset = new Vector3(15, 3, -5);
                cameraOffset = newOffset;
                CameraTarget = targetFishPosition;
                StartCoroutine(PlayCutScene(targetFishCutScene));
                TargetFound = true;
            }
        }
    }

    void SpawnZone(List<Transform> spawnList)
    {
        int zoneCount = spawnList.Count;
        for(int i = 0; i < zoneCount; i++)
        {
            int f = Random.Range(0, fishPrefabs.Count);
            Transform spawn = spawnList[Random.Range(0, spawnList.Count)];
            GameObject fish = Instantiate(fishPrefabs[f], spawn.transform.position, spawn.transform.rotation);
            
            if(fish.TryGetComponent(out DmSoloFish sFish) == true)
            {
                sFish.boundBox.center = spawn.position;
            } 
            if(fish.TryGetComponent(out IbSoloFish xFish) == true)
            {
                xFish.boundBox.center = spawn.position;
            }
            if (fish.TryGetComponent(out Lh_SoloFish yFish) == true)
            {
                yFish.boundBox.center = spawn.position;
            }
            fish.name = fishPrefabs[f].name;
            fishPrefabs.Remove(fishPrefabs[f]);
            spawnList.Remove(spawn);
        }
    }

    public void TogglePlayer(bool toggle)
    {
        CharacterMove.Instance.enabled = toggle;
        MouseLook.Instance.gameObject.SetActive(toggle);
        Interaction.Instance.enabled = toggle;
        Actions.Instance.enabled = toggle; 

    }

    public void PositionCinematicCamera(Transform target)  // set the camera position to point to the target object
    {
        CameraTarget = target;
        if(target != null)
        {
            cutSceneCamera.transform.position = target.position + cameraOffset;
        }
    }    
    
    public void PositionCinematicCamera2()  // set the camera position to point to the target object
    {        
        if(targetFishPosition != null)
        {
            cameraOffset = new Vector3(5, 10, 50);
            cutSceneCamera.transform.position = targetFishPosition.position + cameraOffset;
        }
    }

    public IEnumerator PlayCutScene(PlayableAsset cutScene)
    {
        TogglePlayer(false);  //disables player controlls during the cutscene
        cutSceneCamera.gameObject.SetActive(true);
        cutsceneDirector.playableAsset = cutScene;
        cutsceneDirector.Play();  //plays cutscene
        while(cutsceneDirector.time != cutsceneDirector.playableAsset.duration)
        {
            yield return new WaitForEndOfFrame();
        }
        cutsceneDirector.Stop();
        cutSceneCamera.gameObject.SetActive(false);
        TogglePlayer(true); // re enables player controls

    }
}
