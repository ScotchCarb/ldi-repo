using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed = 3f;

    private Vector3 input;
    private Vector3 motion;
    private CharacterController controller;

    public static PlayerController Instance { get; private set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            enabled = false;
        }
        controller = GetComponent<CharacterController>(); //referance the character controller
    }




    // Update is called once per frame
    void Update()
    {
        motion = Vector3.zero;
        input = new Vector3(Input.GetAxisRaw("Horizontal"),
            Input.GetAxisRaw("Depth"),
            Input.GetAxisRaw("Vertical"));//get inputs
        motion += transform.forward.normalized * input.z;  //forward motion
        motion += transform.right.normalized * input.x;  //horizontal motion
        motion += Vector3.up.normalized * input.y;  //vertical motion
        controller.Move(motion * speed * Time.deltaTime);  //apply movement

    }

    public void MoveToPosition(Vector3 targetPos) //alows the player character to be moved
    {
        controller.enabled = false;  //disabled the players character controlled to not mess with math just in case
        transform.position = targetPos;  //moves hte player
        controller.enabled = true;  //turns player controller back on
    }
}
