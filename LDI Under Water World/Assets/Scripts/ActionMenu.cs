using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActionMenu : MonoBehaviour
{
    public Image[] menuOptions;

    public static ActionMenu Instance { get; private set; }

    private int selection = -1;
    private float timer = -1;

    private void Awake()
    {
        //initialize singleton
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            enabled = false;
        }
    }

    void Start()
    {
        Deactivate();
    }


    void Update()
    {
        if(gameObject.activeSelf == true && timer == -1)
        {
            int axis = Mathf.FloorToInt(Input.GetAxis("Menu Selection"));
            if(axis != 0)
            {
                timer = 0;
                selection += axis;
                if(selection < 0)
                {
                    selection = menuOptions.Length - 1;
                }
                else if (selection > menuOptions.Length - 1)
                {
                    selection = 0;
                }
                UpdateUISelection();
            }
        }

        //timer functionality
        if (timer > -1)
        {
            timer += Time.deltaTime;
            if(timer > 0.2f)
            {
                timer = -1;
            }
        }
    }

    void UpdateUISelection()
    {
        Color selectedOrange;
        Color unselectedBlue;

        ColorUtility.TryParseHtmlString("#FFA047", out selectedOrange);
        ColorUtility.TryParseHtmlString("#3A74A6", out unselectedBlue);

        for (int i = 0; i < menuOptions.Length; i++)
        {
            if( i == selection)
            {
                menuOptions[i].color = selectedOrange;
            }
            else
            {
                menuOptions[i].color = unselectedBlue;
            }
        }
    }

    public void Activate()
    {
        gameObject.SetActive(true);
        selection = 0;
        UpdateUISelection();
    }

    public int Deactivate()
    {
        gameObject.SetActive(false);
        int s = selection;
        selection = -1;
        return s;
    }
}
