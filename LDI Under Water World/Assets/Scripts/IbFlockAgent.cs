using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IbFlockAgent : Fish
{
    public float viewAngle;
    public float smoothDamp;
    public LayerMask obstacleMask;
    public Vector3[] collisionRays;

    private float speed;
    private Vector3 velocity;
    private Vector3 currentAvoidance;
    private IbFlockManager flockInstance;
    private List<IbFlockAgent> cohesionNeighbours = new List<IbFlockAgent>();
    private List<IbFlockAgent> separationNeighbours = new List<IbFlockAgent>();
    private List<IbFlockAgent> alignmentNeighbours = new List<IbFlockAgent>();

    /// <summary>
    /// initializes the agent with the passed flock and speed
    /// </summary>

    public void Initialize(IbFlockManager manager, float initSpeed)
    {
        flockInstance = manager;
        speed = initSpeed;
    }
    /// <summary>
    /// this method calculates and applies the movement for the agents on a grame by frame basis
    /// </summary>
    public void Move()
    {
        FindNeighbours();
        CalculateSpeed();

        Vector3 cohesion = Cohese() * flockInstance.cohesion;
        Vector3 separation = Separate() * flockInstance.separation;
        Vector3 alignment = Align() * flockInstance.alignment;
        Vector3 bounds = Bounds() * flockInstance.bounds;
        Vector3 avoidance = ObstacleAvoidance() * flockInstance.obstacleAvoidance;

        Vector3 motion = cohesion + separation + alignment + bounds + avoidance;
        motion = Vector3.SmoothDamp(transform.forward, motion, ref velocity, smoothDamp);
        motion = motion.normalized * speed;
        if(motion == Vector3.zero)
        {
            motion = transform.forward;
        }
        transform.forward = motion; //rotation
        transform.position += motion * Time.deltaTime; //move position
    }

    /// <summary>
    /// Finds the neigbours for cohesion, alignment and separation
    /// </summary>
    private void FindNeighbours()
    {
        cohesionNeighbours.Clear();
        separationNeighbours.Clear();
        alignmentNeighbours.Clear();

        for(int i = 0; i < flockInstance.SpawnedAgents.Length; i++)
        {
            IbFlockAgent agent = flockInstance.SpawnedAgents[i];
            if(agent != this)
            {
                float distanceSqr = Vector3.SqrMagnitude(agent.transform.position - transform.position);
                if(distanceSqr <= flockInstance.cohesionDistance * flockInstance.cohesionDistance)
                {
                    cohesionNeighbours.Add(agent);
                }
                if(distanceSqr <= flockInstance.separationDistance * flockInstance.separationDistance)
                {
                    separationNeighbours.Add(agent);
                }
                if (distanceSqr <= flockInstance.alignmentDistance * flockInstance.alignmentDistance)
                {
                    alignmentNeighbours.Add(agent);
                }
            }
        }
    }

    /// <summary>
    /// calculates average speed of agent based on neighbours
    /// </summary>
    private void CalculateSpeed()
    {
        if(cohesionNeighbours.Count == 0)
        {
            return;
        }
        speed = 0;
        for(int i= 0; i< cohesionNeighbours.Count; i++)
        {
            speed += cohesionNeighbours[i].speed;
        }
        speed /= cohesionNeighbours.Count;
        speed = Mathf.Clamp(speed, flockInstance.speed.x, flockInstance.speed.y);
    }

    /// <summary>
    /// generates the cohesion vector on a frame by frame basis
    /// </summary>
    /// <returns></returns>
    private Vector3 Cohese()
    {
        Vector3 vector = Vector3.zero;
        int neighboursInView = 0;
        if(cohesionNeighbours.Count == 0)
        {
            return vector;
        }
        for (int i = 0; i < cohesionNeighbours.Count; i++)
        {
            if(VectorInView(cohesionNeighbours[i].transform.position) == true)
            {
                neighboursInView++;
                vector += cohesionNeighbours[i].transform.position;
            }
        }
        if (neighboursInView == 0)
        {
            return vector;
        }
        vector /= neighboursInView;
        vector -= transform.position;
        return vector.normalized;
    }

    /// <summary>
    /// generates the separation vector on a frame by frame basis
    /// </summary>
    /// <returns></returns>
    private Vector3 Separate()
    {
        Vector3 vector = transform.forward;
        if(separationNeighbours.Count == 0)
        {
            return vector;
        }
        int neighboursInView = 0;
        for (int i = 0; i < separationNeighbours.Count; i++)
        {
            if(VectorInView(separationNeighbours[i].transform.position) == true)
            {
                neighboursInView++;
                vector += transform.position - separationNeighbours[i].transform.position;
            }
        }
        vector /= neighboursInView;
        return vector.normalized;
    }

    /// <summary>
    /// generates the alignment vector on a frame by frame basis
    /// </summary>
    /// <returns></returns>
    private Vector3 Align()
    {
        Vector3 vector = transform.forward;
        if (alignmentNeighbours.Count == 0)
        {
            return vector;
        }
        int neighboursInView = 0;
        for (int i = 0; i < alignmentNeighbours.Count; i++)
        {
            if (VectorInView(alignmentNeighbours[i].transform.position) == true)
            {
                neighboursInView++;
                vector += alignmentNeighbours[i].transform.position;
            }
        }
        vector /= neighboursInView;
        return vector.normalized;
    }

    /// <summary>
    /// generates a vector for the agent to stay within the bounds of the flock object on a frame by frame basis
    /// </summary>
    /// <returns></returns>
    private Vector3 Bounds()
    {
        Vector3 centreOffset = flockInstance.transform.position - transform.position;
        bool withinBounds = (centreOffset.magnitude >= flockInstance.boundsDistance * 0.9);
        if(withinBounds == true)
        {
            return centreOffset.normalized;
        }
        return Vector3.zero;


    }

    /// <summary>
    /// generates a vector for the agent to avoid obstacles on a fframe by fframe basis
    /// </summary>
    /// <returns></returns>
    private Vector3 ObstacleAvoidance()
    {
        Vector3 vector = Vector3.zero;
        if(Physics.Raycast(transform.position, transform.forward, out RaycastHit hit, flockInstance.obstacleDistance, obstacleMask) == true)
        {
            vector = AvoidObstacle();
        }
        else
        {
            currentAvoidance = Vector3.zero;
        }
        return vector;
    }

    /// <summary>
    /// checks all collision rays for collisions.
    /// returns the most suitable direction to move in.
    /// </summary>
    /// <returns></returns>
    private Vector3 AvoidObstacle()
    {
        if(currentAvoidance != Vector3.zero)
        {
            if(Physics.Raycast(transform.position, transform.forward, out RaycastHit hit, flockInstance.obstacleDistance, obstacleMask) == false)
            {
                return currentAvoidance;
            }
        }
        float maxDist = int.MinValue;
        Vector3 dir = Vector3.zero;
        for(int i = 0; i < collisionRays.Length; i++)
        {
            Vector3 currentDir = transform.TransformDirection(collisionRays[i].normalized);
            if(Physics.Raycast(transform.position, currentDir, out RaycastHit hit, flockInstance.obstacleDistance, obstacleMask) == true)
            {
                float sqrDist = (hit.point - transform.position).sqrMagnitude;
                if(sqrDist > maxDist)
                {
                    maxDist = sqrDist;
                    dir = currentDir;
                }
            }
            else
            {
                dir = currentDir;
                currentAvoidance = currentDir.normalized;
                return dir.normalized;
            }
        }
        return dir.normalized;
    }

    /// <summary>
    /// returns true if the given position is within agents view radius
    /// </summary>
    /// <param name="position"></param>
    /// <returns></returns>
    private bool VectorInView(Vector3 position)
    {
        return Vector3.Angle(transform.forward, position - transform.position) <= viewAngle;
    }

}
