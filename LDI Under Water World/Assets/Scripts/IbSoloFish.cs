using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IbSoloFish : Fish
{
    public Bounds boundBox;
    public float smoothDamp;
    public Vector3[] collisionRays;
    public float boundsDistance;
    public float obstacleDistance;
    public float speed;
    public LayerMask obstacleMask;

    public float obstacleWeight = 1;
    public float boundsWeight = 1;
    public float targetWeight = 1;


    private Vector3 currentVelocity;
    private Vector3 currentAvoidance;
    private Vector3 targetPosition;
   
    
    void Update()
    {
        
        if(Random.Range(0, 1000) < 5)
        {
            targetPosition = GetRandomPosInBounds();
        }
        MoveStep();
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(boundBox.center, boundBox.size);
        Gizmos.DrawWireSphere(targetPosition, 1);
    }

    private Vector3 GetRandomPosInBounds()
    {
        Vector3 randomCoords = new Vector3(Random.Range(-boundBox.extents.x, boundBox.extents.x), Random.Range(-boundBox.extents.y, boundBox.extents.y), Random.Range(-boundBox.extents.z, boundBox.extents.z));
        return boundBox.center + randomCoords;
    }

    private void MoveStep()
    {
        Vector3 bounds = Bounds() * boundsWeight;
        Vector3 avoidance = ObstacleAvoidance() * obstacleWeight;
        Vector3 target = (targetPosition - transform.position).normalized * targetWeight;

        Vector3 motion = bounds + avoidance + target;
        motion = Vector3.SmoothDamp(transform.forward, motion, ref currentVelocity, smoothDamp);
        motion = motion.normalized * speed;
        if(motion == Vector3.zero)
        {
            motion = transform.forward;
        }
        transform.forward = motion;
        transform.position += motion * Time.deltaTime;
    }

    private Vector3 ObstacleAvoidance()
    {
        Vector3 vector = Vector3.zero;
        float largestDistance = int.MinValue;

        for (int i = 0; i < collisionRays.Length; i++)
        {
            Vector3 currentDir = transform.InverseTransformDirection(collisionRays[i].normalized);
            if (Physics.Raycast(transform.position, currentDir, out RaycastHit hit, obstacleDistance, obstacleMask) == true)
            {
                //Debug.DrawRay(transform.position, currentDir * obstacleDistance, Color.green);
                float sqrDistance = (hit.point - transform.position).sqrMagnitude;
                if(sqrDistance > largestDistance)
                {
                    largestDistance = sqrDistance;
                    vector += -currentDir;
                }
            }
            else
            {
                //Debug.DrawRay(transform.position, currentDir * obstacleDistance, Color.red);
                vector += currentDir;
                currentAvoidance = currentDir.normalized;
            }
        }
        return vector.normalized;
    }

    private Vector3 Bounds() 
    {
        Vector3 centreOffset = boundBox.center - transform.position;
        bool withinBounds = (centreOffset.magnitude >= boundsDistance * 0.9);
        if(withinBounds == true)
        {
            return centreOffset.normalized;
        }
        return Vector3.zero;
    }

}
