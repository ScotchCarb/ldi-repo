using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Lh_ActionMenu : MonoBehaviour
{
    public Image[] menuOption;

    public static Lh_ActionMenu Instance { get; private set; }

    private int selection = -1; 
    private float timer = -1;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            enabled = false;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        Deactivate();
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.activeSelf == true && timer == -1) 
        {
            int axis = Mathf.FloorToInt(Input.GetAxis("Menu Selection")); //Gets input from users and makes sure that the input returns the largest interger int smaller to or equal to axis
            if(axis != 0) //Check is axis does not = 0
            {
                timer = 0; //Set timer to 0
                selection += axis; //Adds selection on to axis
                if(selection < 0) //Sees if slection is less than 0
                {
                    selection = menuOption.Length - 1; //selection will = menuOptions length subtracted by 1
                }
                else if(selection > menuOption.Length - 1) //Checks if selection is greater than menuOptions length subtracted by 1
                {
                    selection = 0; //Sets slection as 0
                }
                updateUISelection();
            }
        }

        //timer functionality
        if(timer > -1) //Check is the timer has been used by seeing if its greater than -1
        {
            timer += Time.deltaTime; //Add Time.deltaTime to timer so its the same amount of time with any computer
            if(timer > 0.2f) //Checks if timer is greater than 0.2f
            {
                timer = -1; //Sets timer as -1
            }
        }
    }

    void updateUISelection()
    {
        Color selectedOrange;
        Color unselectedBlue;

        ColorUtility.TryParseHtmlString("#FFA047", out selectedOrange); //Sets a specific colour for selteced option
        ColorUtility.TryParseHtmlString("#3A74A6", out unselectedBlue);//Sets a specific colour for unslected


        for (int i = 0; i < menuOption.Length; i++) //Check id i is less than the length of menuOptions
        {
            if(i == selection) //If i is = selcted 
            {
                menuOption[i].color = selectedOrange; //Set colour as Orange
            }
            else //If i does not = selection is will rubn
            {
                menuOption[i].color = unselectedBlue; //Set colour as Blue
            }
        }
    }
    public void Activate()
    {
        gameObject.SetActive(true); //Activate gameObject by setting it as true
        selection = 0; //Set the selection as 0
        updateUISelection(); //Calls updateUISelction
    }

    public int Deactivate()
    {
        gameObject.SetActive(false); //Deactivate gameObject by setting it false
        int s = selection;  
        selection = -1;  //slection = -1  
        return s;
    }

}
