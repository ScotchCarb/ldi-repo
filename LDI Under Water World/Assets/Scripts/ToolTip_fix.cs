using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToolTip_fix : MonoBehaviour
{
    public float tooltipTime = 5;

    private Text tooltipText;
    private float timer = -1;

    public static ToolTip_fix Instance { get; private set; }

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            enabled = false;
        }
        tooltipText = GetComponentInChildren<Text>();
    }

    void Start()
    {
        Toggle();
    }

    void Update()
    {
        if(timer > -1)
        {
            timer += Time.deltaTime;
            if (timer > tooltipTime)
            {
                Toggle();
            }
        }
    }

    public void Toggle(string message = null)
    {
        if(message != null)
        {
            tooltipText.text = message;
            gameObject.SetActive(true);
            timer = 0;
        }
        else
        {
            tooltipText.text = null;
            gameObject.SetActive(false);
            timer = -1;
        }
    }
}
