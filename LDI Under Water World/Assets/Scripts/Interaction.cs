using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interaction : MonoBehaviour
{
    public float distance = 2;



    //keyring variable

    public static Interaction Instance { get; private set; }

    private List<Key.KeyType> keyRing = new List<Key.KeyType>();

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            enabled = false;
        }

    }

    void Update()
    {
        if (DmInteractionMenu.Instance.gameObject.activeSelf == false)
        {
            //if action menu and map are not open then execute the following
            if (ActionMenu.Instance.gameObject.activeSelf == false && IbWorldMap.Instance.legend.gameObject.activeSelf == false)
            {

                if (Input.GetButtonDown("Interaction") == true)
                {
                    if (Physics.SphereCast(transform.position, 0.5f, transform.forward, out RaycastHit hit, distance) == true)
                    {
                        // check hit for interaction type
                        if (hit.collider.TryGetComponent(out IInteraction interaction) == true)
                        {
                            //Debug.DrawRay(transform.position, transform.forward * distance, Color.green, 0.25f);
                            //Debug.Log("Interaction detected");
                            DmInteractionMenu.Instance.Activate(interaction);
                        }

                    }

                }
            }

        }
        else
        {
            if(Input.GetButtonUp("Interaction") == true)
            {
                DmInteractionMenu.Instance.Deactivate();
            }
        }
       
    }

    public bool AddKey(Key.KeyType key) //lets you add the key to the keyring if yo udont have it
    {
        if(keyRing.Contains(key) == false)
        {
            keyRing.Add(key);
            return true;
        }
        return false;
    }
    public bool HasKey(Key.KeyType key) // this is incase the player does have it
    {
        return keyRing.Contains(key);
    }
}

public interface IInteraction
{
    string Inspect();
}

public interface IFauna : IInteraction
{
    void Photograph(string folderPath = null);
}

public interface IKey : IInteraction
{
    void Pickup();
}

public interface IGate : IInteraction
{
    bool Unlock();
}

public interface IShip : IInteraction
{
    void Leave();
}