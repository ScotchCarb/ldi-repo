using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IbFlockManager : MonoBehaviour
{
    [Header("Flock Setup")]
    public IbFlockAgent agentPrefab;
    public int size;
    public Vector3 spawnBounds;
    public Vector2 speed;
    [Header("Distances")]
    [Range(0, 10)] public float cohesionDistance;
    [Range(0, 10)] public float separationDistance;
    [Range(0, 10)] public float alignmentDistance;
    [Range(0, 100)] public float boundsDistance;
    [Range(0, 10)] public float obstacleDistance;
    [Header ("Weights")]
    [Range(0, 10)] public float cohesion;
    [Range(0, 10)] public float separation;
    [Range(0, 10)] public float alignment;
    [Range(0, 10)] public float bounds;
    [Range(0, 100)] public float obstacleAvoidance;



    public IbFlockAgent[] SpawnedAgents { get; private set; }


   
    void Start()
    {
        SpawnAgents();
    }

    
    void Update()
    {
       //move each agent
       for(int i =0; i < SpawnedAgents.Length; i++)
        {
            SpawnedAgents[i].Move();
        }
    }

    /// <summary>
    /// This method is used to spawn the flock agents into the world
    /// 
    /// </summary>

    void SpawnAgents()
    {
        SpawnedAgents = new IbFlockAgent[size];
        for (int i = 0; i < size; i++)
        {
            Vector3 random = Random.insideUnitSphere;
            random = new Vector3(random.x * spawnBounds.x, random.y * spawnBounds.y, random.z * spawnBounds.z);
            Vector3 spawnPos = transform.position + random;
            Quaternion rot = Quaternion.Euler(0, Random.Range(0, 360), 0);
            SpawnedAgents[i] = Instantiate(agentPrefab, spawnPos, rot);
            SpawnedAgents[i].Initialize(this, Random.Range(speed.x, speed.y));
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(transform.position, spawnBounds);
    }


}
