using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class MenuManager : MonoBehaviour
{
    [Header("Spawn Settings")]
    public List<GameObject> fishPrefabs;
    public List<Transform> zoneASpawns;



    //public static GameManager Instance { get; private set; }
    public static IFauna Target { get; private set; }
    //public bool TargetFound { get; set; } = false;
    #region Cutscene properties
    public bool OrbitCameraTarget { get; set; } = false;
    public Transform CameraTarget { get; private set; }
    #endregion

    private void Awake()
    {
        SpawnZone(zoneASpawns);
    }



    void SpawnZone(List<Transform> spawnList)
    {
        int zoneCount = spawnList.Count;
        for (int i = 0; i < zoneCount; i++)
        {
            int f = Random.Range(0, fishPrefabs.Count);
            Transform spawn = spawnList[Random.Range(0, spawnList.Count)];
            GameObject fish = Instantiate(fishPrefabs[f], spawn.transform.position, spawn.transform.rotation);
            if (fish.TryGetComponent(out IbSoloFish sFish) == true)
            {
                sFish.boundBox.center = spawn.position;
            }
            fish.name = fishPrefabs[f].name;
            fishPrefabs.Remove(fishPrefabs[f]);
            spawnList.Remove(spawn);
        }
    }
}

  
